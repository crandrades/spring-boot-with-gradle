# ## Alpine based image
 FROM alpine
 RUN apk --update add openjdk8-jre
 VOLUME /tmp
 ADD /build/libs/demo-spring-boot-1.0.0.jar app-spring-boot.jar
 RUN sh -c 'touch /app-spring-boot.jar'
 ENTRYPOINT ["java","-jar","/app-spring-boot.jar"]

# ## Centos based image
# FROM centos
# RUN yum install -y java
# VOLUME /tmp
# ADD /build/libs/demo-spring-boot-0.1.0.jar app-spring-boot.jar
# RUN sh -c 'touch /app-spring-boot.jar'
# ENTRYPOINT ["java","-jar","/app-spring-boot.jar"]