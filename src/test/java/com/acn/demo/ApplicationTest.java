package com.acn.demo;

import static org.assertj.core.api.Assertions.assertThat;

import com.acn.demo.controllers.HelloController;
import com.acn.demo.controllers.RandomController;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTest {

    @Autowired
    private HelloController helloController;

    @Autowired
    private RandomController randomController;

    @Test
    public void shouldLoadContextAndControllers() throws Exception {
        assertThat(helloController).isNotNull();
        assertThat(randomController).isNotNull();
    }

}