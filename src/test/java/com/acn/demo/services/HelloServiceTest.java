package com.acn.demo.services;

import static org.assertj.core.api.Assertions.assertThat;

import com.acn.demo.dtos.MessageDTO;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HelloServiceTest {

    @Autowired
    private HelloService helloService;

    @Value("${properties.helloService.statusMessageOk}")
    private String statusMessageOk;

    @Value("${properties.helloService.greetingMessage}")
    private String greetingMessage;

    @Test
    public void shouldGetNotNullStatusMessageOk() throws Exception {
        assertThat(statusMessageOk).isNotEmpty();
    }

    @Test
    public void shouldGetNotNullGreetingMessage() throws Exception {
        assertThat(greetingMessage).isNotEmpty();
    }

    @Test
    public void shouldGetHelloMessage() throws Exception {
        MessageDTO serviceResponse = helloService.greetings();
        assertThat(serviceResponse.getStatus()).isEqualTo(statusMessageOk);
        assertThat(serviceResponse.getMessage()).isEqualTo(greetingMessage);
    }

}