package com.acn.demo.controllers;

import com.acn.demo.dtos.MessageDTO;
import com.acn.demo.services.RandomService;

import org.assertj.core.api.Assertions;

import org.hamcrest.Matchers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.Mock;
import org.mockito.Mockito;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.is;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RandomControllerTest {

    @Value("${properties.randomService.minRandomValue}")
    private int min;

    @Value("${properties.randomService.statusMessageOk}")
    private String statusMessageOk;

    @Value("${properties.randomService.okMessage}")
    private String okMessage;

    @Value("${properties.randomService.statusMessageNok}")
    private String statusMessageNok;

    @Value("${properties.randomService.errorMessage}")
    private String errorMessage;

    private MockMvc mvc;

    @Mock
    private RandomService randomServiceMock;

    private RandomController randomController;

    @Before
    public void setup() {
        randomController = new RandomController(randomServiceMock);
        this.mvc = MockMvcBuilders.standaloneSetup(randomController).build();
    }

    @Test
    public void shouldReadMinRandomValueEqualOrGreaterThanZeroFromProperties() throws Exception {
        Assertions.assertThat(min).isNotNull();
        Assertions.assertThat(min).isGreaterThanOrEqualTo(0);
    }

    @Test
    public void shouldReadNotNullStatusMessageOkFromProperties() throws Exception {
        Assertions.assertThat(statusMessageOk).isNotEmpty();
    }

    @Test
    public void shouldReadNotNullOkMessageFromProperties() throws Exception {
        Assertions.assertThat(okMessage).isNotEmpty();
    }

    @Test
    public void shouldReadNotNullStatusMessageNokFromProperties() throws Exception {
        Assertions.assertThat(statusMessageNok).isNotEmpty();
    }

    @Test
    public void shouldReadNotNullErrorMessageFromProperties() throws Exception {
        Assertions.assertThat(errorMessage).isNotEmpty();
    }

    @Test
    public void shouldGetOkResponseWithRandomNumberMessage() throws Exception {
        int max = min + 1;
        Mockito.when(randomServiceMock.random(max))
                .thenReturn(new MessageDTO(statusMessageOk, String.format(okMessage, min, max, max)));

        ResultActions result = mvc
                .perform(MockMvcRequestBuilders.get("/random/" + max).accept(MediaType.APPLICATION_JSON));
        result.andExpect(MockMvcResultMatchers.status().isOk()).andExpect(jsonPath("$.status", is(statusMessageOk)))
                .andExpect(jsonPath("$.message", Matchers.containsString(String.format(okMessage, min, max, ""))));
    }

    @Test
    public void shouldGetOkResponseWithErrorMessage() throws Exception {
        int max = min - 1;
        Mockito.when(randomServiceMock.random(max))
                .thenReturn(new MessageDTO(statusMessageNok, String.format(errorMessage, min)));

        ResultActions result = mvc
                .perform(MockMvcRequestBuilders.get("/random/" + max).accept(MediaType.APPLICATION_JSON));
        result.andExpect(MockMvcResultMatchers.status().isOk()).andExpect(jsonPath("$.status", is(statusMessageNok)))
                .andExpect(jsonPath("$.message", Matchers.containsString(String.format(errorMessage, min))));
    }

    @Test
    public void shouldGetErrorResponse() throws Exception {
        int max = min + 5;
        Mockito.when(randomServiceMock.random(max)).thenThrow(NullPointerException.class);

        ResultActions result = mvc
                .perform(MockMvcRequestBuilders.get("/random/" + max).accept(MediaType.APPLICATION_JSON));
        result.andExpect(MockMvcResultMatchers.status().isInternalServerError())
                .andExpect(MockMvcResultMatchers.content().string(Matchers.isEmptyOrNullString()));
    }

}