package com.acn.demo.controllers;

import com.acn.demo.dtos.MessageDTO;
import com.acn.demo.services.HelloService;

import org.assertj.core.api.Assertions;

import org.hamcrest.Matchers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.Mock;
import org.mockito.Mockito;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.is;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class HelloControllerTest {

    @Value("${properties.helloService.statusMessageOk}")
    private String statusMessageOk;

    @Value("${properties.helloService.greetingMessage}")
    private String greetingMessage;

    private MockMvc mvc;

    @Mock
    private HelloService helloServiceMock;

    private HelloController helloController;

    @Before
    public void setup() {
        helloController = new HelloController(helloServiceMock);
        // MockitoAnnotations.initMocks(this);
        this.mvc = MockMvcBuilders.standaloneSetup(helloController).build();
    }

    @Test
    public void shouldReadNotNullStatusMessageOkFromProperties() throws Exception {
        Assertions.assertThat(statusMessageOk).isNotEmpty();
    }

    @Test
    public void shouldReadNotNullGreetingMessageFromProperties() throws Exception {
        Assertions.assertThat(greetingMessage).isNotEmpty();
    }

    @Test
    public void shouldGetOkResponseWithHelloMessage() throws Exception {
        Mockito.when(helloServiceMock.greetings()).thenReturn(new MessageDTO(statusMessageOk, greetingMessage));

        ResultActions result = mvc
                .perform(MockMvcRequestBuilders.get("/greetings/").accept(MediaType.APPLICATION_JSON));
        result.andExpect(MockMvcResultMatchers.status().isOk()).andExpect(jsonPath("$.status", is(statusMessageOk)))
                .andExpect(jsonPath("$.message", is(greetingMessage)));
    }

    @Test
    public void shouldGetErrorResponse() throws Exception {
        Mockito.when(helloServiceMock.greetings()).thenThrow(NullPointerException.class);

        ResultActions result = mvc
                .perform(MockMvcRequestBuilders.get("/greetings/").accept(MediaType.APPLICATION_JSON));
        result.andExpect(MockMvcResultMatchers.status().isInternalServerError())
                .andExpect(MockMvcResultMatchers.content().string(Matchers.isEmptyOrNullString()));
    }

}