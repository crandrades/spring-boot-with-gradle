package com.acn.demo.services;

import com.acn.demo.dtos.MessageDTO;

public interface HelloService {

    /**
     * Returns a simple greeting text wrapped in a MessageDTO
     * 
     * @return {@link MessageDTO} A simple greeting message
     */
    MessageDTO greetings();

}