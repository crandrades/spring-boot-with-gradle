package com.acn.demo.services.impl;

import com.acn.demo.dtos.MessageDTO;
import com.acn.demo.services.HelloService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service("HelloService")
public class HelloServiceImpl implements HelloService {

    @Value("${properties.helloService.statusMessageOk}")
    private String statusMessageOk;

    @Value("${properties.helloService.greetingMessage}")
    private String greetingMessage;

    /**
     * Returns a simple greeting text wrapped in a MessageDTO
     * 
     * @return {@link MessageDTO} A simple greeting message
     */
    @Override
    public MessageDTO greetings() {
        return new MessageDTO(statusMessageOk, greetingMessage);
    }

}