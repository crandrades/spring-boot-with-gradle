package com.acn.demo.services.impl;

import java.util.concurrent.ThreadLocalRandom;

import com.acn.demo.dtos.MessageDTO;
import com.acn.demo.entities.RandomGenerated;
import com.acn.demo.repositories.RandomGeneratedRepository;
import com.acn.demo.services.RandomService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service("RandomService")
public class RandomServiceImpl implements RandomService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RandomServiceImpl.class);

    private final RandomGeneratedRepository randomGeneratedRepository;

    @Value("${properties.randomService.minRandomValue}")
    private int min;

    @Value("${properties.randomService.statusMessageOk}")
    private String statusMessageOk;

    @Value("${properties.randomService.okMessage}")
    private String okMessage;

    @Value("${properties.randomService.statusMessageNok}")
    private String statusMessageNok;

    @Value("${properties.randomService.errorMessage}")
    private String errorMessage;

    @Autowired
    public RandomServiceImpl(RandomGeneratedRepository randomGeneratedRepository) {
        this.randomGeneratedRepository = randomGeneratedRepository;
    }

    /**
     * Generates a random number between zero and a given max number
     * 
     * @param max {@link int} An int with the max value for the random number
     * @return {@link MessageDTO} A message with a text with the random number
     *         generated or an error text if the given number is lower than zero
     */
    public MessageDTO random(int max) {
        if (max < min) {
            String message = String.format(errorMessage, min);

            RandomGenerated randomGenerated = new RandomGenerated(min, max, -1, message, statusMessageNok);
            this.saveRandomGenerated(randomGenerated);

            return new MessageDTO(statusMessageNok, message);
        } else {
            Integer random = ThreadLocalRandom.current().nextInt(min, max + 1);
            String message = String.format(okMessage, min, max, random);

            RandomGenerated randomGenerated = new RandomGenerated(min, max, random, message, statusMessageOk);
            this.saveRandomGenerated(randomGenerated);

            return new MessageDTO(statusMessageOk, message);
        }
    }

    /**
     * Saves a random number generated into the database corresponding table
     * 
     * @param randomGenerated {@link RandomGenerated} The random number generated
     *                        with its message
     */
    private void saveRandomGenerated(RandomGenerated randomGenerated) {
        LOGGER.info(randomGenerated.getMessage(), randomGeneratedRepository.save(randomGenerated));
    }

}