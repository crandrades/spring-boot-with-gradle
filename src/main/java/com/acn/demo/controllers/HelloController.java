package com.acn.demo.controllers;

import org.springframework.web.bind.annotation.RestController;

import com.acn.demo.dtos.MessageDTO;
import com.acn.demo.services.HelloService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
@RequestMapping("greetings")
public class HelloController {

    private final HelloService helloService;

    @Autowired
    public HelloController(final HelloService helloService) {
        this.helloService = helloService;
    }

    /**
     * An interface that could be consumed in the root path for getting a greeting
     * message
     * 
     * @return {@link MessageDTO} A simple greeting message
     */
    @RequestMapping(path = { "/" }, method = { RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<MessageDTO> index() {
        try {
            MessageDTO controllerResponse = helloService.greetings();
            return new ResponseEntity<>(controllerResponse, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}