package com.acn.demo.controllers;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.acn.demo.dtos.MessageDTO;
import com.acn.demo.services.RandomService;

@RestController
@RequestMapping("random")
public class RandomController {

    private final RandomService randomService;

    @Autowired
    public RandomController(RandomService randomService) {
        this.randomService = randomService;
    }

    /**
     * An interface that generates a random number between zero and a given max
     * value
     * 
     * @param max {@link int} A max value given as a path variable in the interface
     *            invocation
     * @return {@link MessageDTO} A message with a text with the random number
     *         generated or an error text if the given number is lower than zero
     */
    @RequestMapping(path = { "/{max}" }, method = { RequestMethod.GET }, produces = {
            MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<MessageDTO> index(@PathVariable int max) {
        try {
            MessageDTO controllerResponse = randomService.random(max);
            return new ResponseEntity<>(controllerResponse, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}