package com.acn.demo;

import java.text.SimpleDateFormat;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RestController;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class Application {

  private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

  private final static String ENVIRONMENT_PROD = "prod";

  @Value("${environment}")
  private String environment;

  /**
   * Basic Swagger configuration
   * 
   * @return {@link Docket} Swagger configuration
   */
  @Bean
  public Docket apiDocket() {
    return new Docket(DocumentationType.SWAGGER_2).select()
        .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class)).paths(PathSelectors.any()).build();
  }

  /**
   * A bean that executes at application start up and retrieves all the beans that
   * were created either by the app or were automatically added thanks to Spring
   * Boot
   * 
   * @param ctx {@link ApplicationContext} The application context just
   *            initialized
   * @return {@link CommandLineRunner} A CommandLineRunner executed
   */
  @Bean
  public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
    return args -> {
      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
      LOGGER.info(String.format("Context with profile '%s' started up at: %s", environment,
          format.format(ctx.getStartupDate())));

      if (!ENVIRONMENT_PROD.equalsIgnoreCase(environment)) {
        LOGGER.info("Let's inspect the beans provided by Spring Boot:");

        String[] beanNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanNames);

        for (String beanName : beanNames) {
          LOGGER.info("  " + beanName);
        }
      }
    };
  }

  /**
   * Main routine - It starts the application context
   */
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

}