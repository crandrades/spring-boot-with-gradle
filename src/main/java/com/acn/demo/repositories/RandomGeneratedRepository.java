package com.acn.demo.repositories;

import com.acn.demo.entities.RandomGenerated;

import org.springframework.data.repository.CrudRepository;

public interface RandomGeneratedRepository extends CrudRepository<RandomGenerated, Long> {

}