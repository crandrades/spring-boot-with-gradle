# Index

 1. [Description](#description)
 2. [Prerequisites](#prerequisites)
 3. [Components](#components)
 4. [Project properties](#project-properties)
 5. [Java Properties](#java-properties)
 6. [Embedded database (H2)](#embedded-database-(h2))
 7. [Structure](#structure)
 8. [Profiles](#profiles)
 9. [Test](#test)
10. [Compilation](#compilation)
11. [Reports and coverage rules](#reports-and-coverage-rules)
12. [Execution](#execution)
13. [Accessing](#accessing)
14. [Docker](#docker)

---
## Description

This source code contains a template for a aplication that exposes a REST api using the [spring-boot][spring_boot_url] framework. Consider this project as a demo template since it uses an embeded database as data repository.
The source code also contains scripts for using [Gradle][gradle_url] as a build automation system, as well as a [Dockerfile][dockerfile_url] for creating a docker image and deploying the JAR as a containerized application.

---
## Prerequisites

The first required prerequisite is having a [Java JDK or JSE][java_jdk_jse_url] installed. The recomendation is to install the _Java SE Development Kit 8_.

The second required prerequisite is having Gradle installed. A guide for installing Gradle is provided in the official [Gradle documentation][gradle_install_url].

An __optional__ prerequisite is to have [Docker][docker_url] installed. A detailed guide is provided in the official [Docker CE][docker_ce_url] documentation. A simple way of having Docker is to install [Docker desktop][docker_desktop_url].

---
## Components

1. Java application built with [spring-boot][spring_boot_url].
2. Dependencies and configurations for an embeded [H2 database][h2_database_url].
3. Unit test built with [spring-test][spring_test_url], [Junit][junit_url] and [Mockito][mockito_url], and some utilities as [AssertJ][assertj_url] and [Java Hamcrest][hamcrest_java_url].
4. API documentation is provided through [Swagger][swagger_url].
5. [Gradle][gradle_url] scripts for compilation, executing unit test and validating coverage with [Jacoco][jacoco_url].
6. [Dockerfile][dockerfile_url] for docker image creation using [Alpine Linux][alpine_linux_url] as container SO (see [Alpine Linux in Docker Hub][alpine_docker_hub_url]).

---
## Project properties

Project properties are defined in `.gradle` files. There are three of this gradle files:
 - __build.gradle:__ Main gradle script which defines several project properties.
    - The spring-framework version.
    - The project dependencies and the dependencies repositories.
    - The Java version (as _sourceCompatibility_ and _targetCompatibility_).
    - Some [gradle plugins][gradle_plugins_url].
    - Specifies the gradle scripts for compilation and test execution (_project.gradle_ and _test.gradle_).

 - __project.gradle:__ This gradle script defines the source directory tree for Java source code, unit test source code, and project resources. It also defines the project version by using the [SemVer][semver_url] specification.

 - __test.gradle:__ This gradle script has several test configurations for [JUnit][junit_url] and [Jacoco][jacoco_url] test reports.
    - Exclusions for Jacoco validations.
    - [Jacoco coverage][jacoco_coverage_url] rules for build acceptance (it considers _lines coverage ratio_ and _branches coverage ratio_).
    - JUnit reports destiny folder.
    - Jacoco reports destiny folder.

---
## Java Properties

Java project configurations are set at `src/main/resources/application.yml`. This configuration file has a few relevant points:

 - It has two different profiles separated by a `---` line, which defines a 'document separation' (see [YAML specification][yaml_specification_url]). These two profiles are _test_ and _prod_, both defined under the path `spring:profiles`. In order to active one of these profiles, set its name under the path `spring:profiles:active`.
 ```yml
 spring:
   profiles: test
```
```yml
spring:
   profiles: prod
```
```yml
spring:
   profiles:
      active: prod
```

 - It also has configuration properties for an embeded [H2 database][h2_database_url]. These configurations are the definition of the _datasource_, the _jpa_, and the availability of the default H2 console.
```yml
datasource:
   dataSourceClassName: org.h2.jdbcx.JdbcDataSource
   url: jdbc:h2:mem:db
   databaseName:
   serverName:
   username:
   password:
jpa:
   database-platform: org.hibernate.dialect.H2Dialect
   database: H2
   openInView: false
   show_sql: true
   generate-ddl: true
   hibernate:
      ddl-auto: create-drop
      naming-strategy: org.hibernate.cfg.EJB3NamingStrategy
   properties:
      hibernate.cache.use_query_cache: false
      hibernate.generate_statistics: true
h2:
   console:
      enabled: true
```

---
## Embedded database (H2)

An embedded H2 database is provided as part of this project. As it is mentioned in the previous section, the configuration for this in-memory database is set in the `src/main/resources/application.yml` file. The default H2 console can be accesed in the URL `http://localhost:<PORT>/h2-console/`, being _\<PORT>_ the port used to initiate the application execution (see [execution section](#execution)).

Note in the `application.yml` file the use of the JDBC URL as `jdbc:h2:mem:db`. This URL will be used by [Hibernate][hibernate_url] in order to connect and operate over the H2 database. When using the H2 console, this URL must be provided to see all the database objects created by the application.
```yml
datasource:
   url: jdbc:h2:mem:db
```

To keep the database alive as long as the virtual machine, add `;B_CLOSE_DELAY=-1` at the end of this JDBC URL.
```yml
datasource:
   url: jdbc:h2:mem:db;B_CLOSE_DELAY=-1
```

Finally, any SQL script that is needed to run at the application startup, can be added in the `src/main/resources/data.sql`. This SQL script will be executed over the H2 database by using the _datasource_ configuration when the application is started.

---
## Structure

This project uses the _Controller-Service-Repository_ pattern. All of the Java classes implementing any of these layers are under a folder that defines its goal. For example, we have the `src/main/java/com/acn/demo/controllers` folder, as well as the `src/main/java/com/acn/demo/services` and `src/main/java/com/acn/demo/repositories` folders.

Additionally, other Java objects usually used with the _Controller-Service-Repository_ pattern are declared under the `src/main/java/com/acn/demo/entities` and `src/main/java/com/acn/demo/dtos` folders.

---
## Profiles

As it is stated in the [Java properties](#java-properties) section, two profiles are defined, and one of them must be activated. By following the same specification structure, more than two profiles can be declared. The only restriction is that __only one profile can be active__. Profiles can be used to use different properties in different environments. As a example, we use a few different properties that make the _greeting_ and the _random_ interfaces to operate differently based on the profile that is currently active.

__test profile:__
```yml
properties:
    helloService:
        greetingMessage: 'Greetings from Spring Boot in test environment!'
    randomService:
        minRandomValue: 10
```

__prod profile:__
```yml
properties:
    helloService:
        greetingMessage: 'Greetings from Spring Boot in production environment!'
    randomService:
        minRandomValue: 20
```

---
## Test

Unit tests are under the `src/test` folder. They are built using the [org.springframework.test.context][springboot_test_context_url] and the [org.springframework.test.web][springboot_test_web_url] packages of the Springboot Framework, [Junit][junit_url] and [Mockito][mockito_url], and some utilities as [AssertJ][assertj_url] and [Java Hamcrest][hamcrest_java_url].

These unit test generate a JUnit and Jacoco reports, whose are used with some coverage rules in order to evaluate the build acceptance (see [project properties](#project-properties) section).

---
## Compilation

In order to compile the application, using the test coverage rules for build acceptance, run the following command from the _root_ folder:
`gradle clean build`

In order to compile the application, __without__ using the test coverage rules for build acceptance, run the following command from the _root_ folder:
`gradle clean build -x test`

Example:
```sh
spring-boot-with-gradle:~$ gradle clean build
```

---
## Reports and coverage rules

The `test.gradle` file defines the output folders for JUnit and Jacoco reports.

__JUnit reports:__
```gradle
tasks.withType(Test) {
    reports.html.destination = file("reports/junitHtml")
}
```

__Jacoco reports:__
```gradle
jacocoTestReport {
    reports {
        html.destination = file("reports/jacocoHtml")
    }
}
```

It also defines the coverage rules for build acceptance. If these thresholds are not meet, then the application build will fail.
```gradle
jacocoTestCoverageVerification {
    violationRules {
        rule {
            enabled = true
            limit {
                counter = 'LINE'
                value = 'COVEREDRATIO'
                minimum = 0.85
            }
        }
        rule {
            enabled = true
            limit {
                counter = 'BRANCH'
                value = 'COVEREDRATIO'
                minimum = 0.75
            }
        }
    }
}
```

---
## Execution

In order to start the application using the default port (8080), use the following command from the _root_ folder: `java -jar <JAR_LOCATION>`

In order to start the application using __another__ port, use the following command from the _root_ folder: `java -jar <JAR_LOCATION> --server.port=<PORT>`

Example:
```sh
spring-boot-with-gradle:~$ java -jar build/libs/demo-spring-boot-1.0.0.jar --server.port=7080
```
Note that, by default and after a sucessful compilation, _\<JAR_LOCATION>_ should be `build/libs/demo-spring-boot-1.0.0.jar`

---
## Accessing

Once the application was succesfully compiled and executed, it will exposes two interfaces.

- __Greeting interface:__
It can be accessed at `http://localhost:<PORT>/greetings/`

- __Random interface:__
It can be accessed at `http://localhost:<PORT>/random/<NUMBER>`

   Example for _**Random interface:**_ 
   - `http://localhost:8080/random/10` - It should send an error message if the _test_ profile is active.
   - `http://localhost:8080/random/50` - It should send an OK message with any of the provided profiles being active.

Additionally, the default H2 console can be accessed at `http://localhost:<PORT>/h2-console/`, while the Swagger documentation can be accessed at `http://localhost:<PORT>/swagger-ui.html`

---
## Docker

### Docker image creation
A [Dockerfile][dockerfile_url] for creating a docker image and deploying the JAR as a containerized application is provided. Once the application was succesfully compiled (see [compilation](#compilation) section), the docker image can be created by using the following command from the _root_ folder: `docker build -t <IMAGE_NAME>:<TAG> .`

Example (do not forget the ending dot!):
```sh
spring-boot-with-gradle:~$ docker build -t spring-boot-demo-app:1.0.0 .
```

### Listing docker images
Once the docker image was successfully created, it can be listed with the command `docker images` (note that also an _alphine_ image will be created as dependency).

### Container startup
Running a container with the application can be achieved by executing the following command: `docker run --name <NAME> -p <HOST_PORT>:<CONTAINER_PORT> -d <IMAGE_NAME>:<TAG>`.

   - `<NAME>`: An identification name for the container.
   - `-p <HOST_PORT>:<CONTAINER_PORT>`: It maps a host port to a port exposed by the container.
   - `-d`: It runs a _detached_ container, so the terminal input-output is not linked with the container.
   - `<IMAGE_NAME>:<TAG>`: It should match the name and tag of the docker image.

_Example:_
```sh
spring-boot-with-gradle:~$ docker run --name spring-demo-app -p 6080:8080 -d spring-boot-demo-app:1.0.0
```

That way, the application API interfaces can be accessed at `http://localhost:6080` and `http://localhost:6080/random/50`.

### Listing running containers
The status of the container can be seen by using `docker ps` or `docker stats`.

### Stop container
In order to stop the container, the following commands can be used: `docker stop <CONTAINER_ID>` or `docker stop <NAME>`.

_Example:_
```sh
spring-boot-with-gradle:~$ docker stop spring-demo-app
```

### Delete container
For deleting the container, the following commands can be used: `docker rm <CONTAINER_ID>` or `docker stop <NAME>`.

_Example:_
```sh
spring-boot-with-gradle:~$ docker rm spring-demo-app
```

### Delete docker images
Finally, for deleting the images created, the following commands should be used: `docker rmi <IMAGE_NAME>:<TAG>`and `docker rmi alphine:latest`.

_Example:_
```sh
spring-boot-with-gradle:~$ docker rmi spring-boot-demo-app:1.0.0
```
```sh
spring-boot-with-gradle:~$ docker rmi alphine:latest
```

---
[gradle_url]: https://gradle.org/
[gradle_install_url]: https://gradle.org/install/
[gradle_plugins_url]: https://docs.gradle.org/current/userguide/plugins.html
[spring_boot_url]: https://spring.io/projects/spring-boot
[h2_database_url]: http://www.h2database.com
[spring_test_url]: https://docs.spring.io/spring/docs/current/spring-framework-reference/testing.html
[junit_url]: https://junit.org/
[mockito_url]: https://site.mockito.org/
[assertj_url]: http://joel-costigliola.github.io/assertj/
[hamcrest_java_url]: http://hamcrest.org/JavaHamcrest/
[jacoco_url]: https://www.eclemma.org/jacoco/
[jacoco_coverage_url]: https://www.eclemma.org/jacoco/trunk/doc/counters.html
[docker_url]: https://www.docker.com/
[dockerfile_url]: https://docs.docker.com/engine/reference/builder/
[docker_ce_url]: https://docs.docker.com/install/
[docker_desktop_url]: https://www.docker.com/products/docker-desktop
[alpine_linux_url]: https://alpinelinux.org/
[alpine_docker_hub_url]: https://hub.docker.com/_/alpine
[semver_url]: https://semver.org/
[yaml_specification_url]: https://yaml.org/spec/
[hibernate_url]: http://hibernate.org/
[springboot_test_context_url]: https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/test/context/package-summary.html
[springboot_test_web_url]: https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/test/web/package-summary.html
[java_jdk_jse_url]: https://www.oracle.com/technetwork/java/javase/downloads/index.html
[swagger_url]: https://swagger.io/